export const formStructure = [{
    name: "age",
    isMulti: true,
    type: "select",
    isValid: true,
    validation: "required",
    options: [{
            value: null,
            text: "Select age range"
        },
        {
            value: "15-20",
            text: "15 - 20"
        },
        {
            value: "20-25",
            text: "20 - 25"
        }
    ],
    value: null
}]